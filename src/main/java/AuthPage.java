import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class AuthPage extends SearchPage {
    SelenideElement login = $("input[data-qa='login']");
    SelenideElement password = $("input[data-qa='password']");
    SelenideElement inner = $("button[data-qa='inner']");
    SelenideElement name = $("span[data-qa='name']");

    @Step("Открытие страницы")
    public void openPage() {
        open("https://gossort.ultra-production.ru/login", "", "dev", "garpixdev");
    }

    @Step("Ввод логина: {username} и пароля: {password}")
    public void login(String username, String password) {
        this.login.setValue(username);
        this.password.setValue(password);
        this.inner.shouldHave(visible).click();
    }

    @Step("Проверка успешной авторизации: {fio}")
    public void checkAuth(String fio) {
        this.name.shouldHave(text(fio));
    }
}