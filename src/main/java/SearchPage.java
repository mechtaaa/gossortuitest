
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class SearchPage {

    SelenideElement buttonMap = $("a[data-qa='buttonMap']");
    SelenideElement pageName = $("h3[data-qa='pageName']");
    SelenideElement buttonNewRegion = $("button[data-qa='buttonNewRegion']");
    SelenideElement checkLocation = $("p[data-qa='checkLocation']");
    SelenideElement deleteLocation = $("button[data-qa='getDeleteLocation']");
   SelenideElement confirmDeleteLocation = $("button[data-qa='confirmDelete']");

    @Step("Открыть влкадку 'Карта ГСУ'")
    public void clickPageRegion() {
        buttonMap.shouldBe(visible).click();
    }

    @Step("Проверка открытии страницы 'Карта ГСУ'")
    public void checkPageName() {
        pageName.shouldHave(text("Участки"));
    }

    @Step("Переход на страницу создании Участка")
    public void clickButtonNewRegion() {
        buttonNewRegion.shouldBe(visible).click();
    }

    @Step("Проверить созданный Участок: {createdLocation}")
    public void checkCreatedLocation(String createdLocation) {
        checkLocation.shouldHave(text(createdLocation)).click();
    }

    @Step("Удаление участка")
    public void deleteCreatedLocation() {
        deleteLocation.should(Condition.visible).click();
        confirmDeleteLocation.shouldBe(visible).click();
    }

    @Step("Проверка, что участок удалён")
    public void checkDeletedLocation() {
        Selenide.refresh();
        checkLocation.shouldNot(exist);
    }
}