import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;

public class CreateLocationPage extends SearchPage {
    SelenideElement mapName = $("input[data-qa='mapName']");
    ElementsCollection addFilters  = $$("input[data-qa='autocomplite']");
    SelenideElement buttonSaveLocation = $("button[data-qa='buttonSaveLocation']");


    public void createLocation(String nameValue) {
        mapName.sendKeys(nameValue);
        sleep(1000);
        if (addFilters.size() >= 3) {
            SelenideElement mapRegion = addFilters.get(0);
            mapRegion.sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER);
            sleep(1000);
            SelenideElement mapFilial = addFilters.get(1);
            mapFilial.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
            sleep(1000);
            SelenideElement mapGsu = addFilters.get(2);
            mapGsu.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
        } else {
            System.out.println("Не удалось найти нужное количество полей.");
        }
    }

    @Step("Сохраение участка")
    public void clickButtonSaveLocation() {
        buttonSaveLocation.shouldHave(visible).click();
    }
}