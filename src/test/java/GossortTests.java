import io.qameta.allure.Description;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class GossortTests extends BasePageTest {
    AuthPage authPAge = new AuthPage();
    SearchPage searchPage = new SearchPage();
    CreateLocationPage createRegionPage = new CreateLocationPage();

    @BeforeMethod
    @Description("Проверка успешной авторизации")
    public void authTest() {
        authPAge.openPage();
        authPAge.login("testA", "Qwerty123456!");
        authPAge.checkAuth("Петр Петров");
    }

    @Test
    @Description("Проверка создании участка")
    public void createRegion() {
        searchPage.clickPageRegion();
        searchPage.checkPageName();
        searchPage.clickButtonNewRegion();
        createRegionPage.createLocation("Авто Участок");
        createRegionPage.clickButtonSaveLocation();
        searchPage.checkCreatedLocation("Авто Участок");
    }

    @Test
    @Description("Проверка удалении участка")
    public void deleteRegion() {
        searchPage.clickPageRegion();
        searchPage.checkPageName();
        searchPage.checkCreatedLocation("Авто Участок");
        searchPage.deleteCreatedLocation();
        searchPage.checkDeletedLocation();
    }
}
